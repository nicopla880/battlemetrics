import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../lib/core/usecases/usecase.dart';
import '../../../../../lib/features/servers/domain/entities/server.dart';
import '../../../../../lib/features/servers/domain/repositories/server_repository.dart';
import '../../../../../lib/features/servers/domain/usecases/get_server_list.dart';

class MockServerRepository extends Mock implements ServerRepository {}

void main() {
  GetServerList usecase;
  MockServerRepository mockServerRepository;

  setUp(() {
    mockServerRepository = MockServerRepository();
    usecase = GetServerList(mockServerRepository);
  });

  final tServerList = [
    Server(
        id: 'IDTEST',
        name: 'TEST SERVER',
        address: "vanilla.test.com",
        ip: "208.103.169.97",
        port: 24870,
        onlinePlayers: 343,
        maxPlayers: 355,
        rank: 2,
        status: "online",
        detail: null,
        createdAt: null,
        updatedAt: null)
  ];

  test('Should get server list from the repository', () async {
    // arrange
    when(mockServerRepository.getServerList())
        .thenAnswer((_) async => Right(tServerList));
    // act
    final result = await usecase(NoParams());
    // assert
    expect(result, Right(tServerList));
    verify(mockServerRepository.getServerList());
    verifyNoMoreInteractions(mockServerRepository);
  });
}
