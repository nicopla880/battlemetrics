import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../lib/features/servers/domain/entities/server.dart';
import '../../../../../lib/features/servers/domain/repositories/server_repository.dart';
import '../../../../../lib/features/servers/domain/usecases/get_server_by_id.dart';

class MockServerRepository extends Mock implements ServerRepository {}

void main() {
  GetServerById usecase;
  MockServerRepository mockServerRepository;

  setUp(() {
    mockServerRepository = MockServerRepository();
    usecase = GetServerById(mockServerRepository);
  });

  final String tId = 'IDTEST';
  final tServer = Server(
      id: 'IDTEST',
      name: 'TEST SERVER',
      address: "vanilla.test.com",
      ip: "208.103.169.97",
      port: 24870,
      onlinePlayers: 343,
      maxPlayers: 355,
      rank: 2,
      status: "online",
      detail: null,
      createdAt: null,
      updatedAt: null);

  test('Should get server from the repository', () async {
    // arrange
    when(mockServerRepository.getServer(any))
        .thenAnswer((_) async => Right(tServer));
    // act
    final result = await usecase(Params(tId));
    // assert
    expect(result, Right(tServer));
    verify(mockServerRepository.getServer(tId));
    verifyNoMoreInteractions(mockServerRepository);
  });
}
