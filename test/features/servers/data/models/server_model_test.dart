import 'package:flutter_test/flutter_test.dart';

import '../../../../../lib/features/servers/data/models/server_model.dart';
import '../../../../../lib/features/servers/domain/entities/server.dart';

void main() {
  final tServerModel = ServerModel(
      id: 'IDTEST',
      name: 'TEST SERVER',
      address: 'vanilla.adress.com',
      ip: "190.0.0.1",
      port: 25480,
      onlinePlayers: 100,
      maxPlayers: 300,
      rank: 40,
      status: "online",
      detail: null,
      createdAt: null,
      updatedAt: null);

  test('Should be a subclass of Server Entity', () async {
    // assert
    expect(tServerModel, isA<Server>());
  });

  //CONTINUAR
  group('fromJson ', () {
    test('Should return a valid model when the JSON is complete', () {
      // arrange

      // act

      // assert
    });
  });
}
