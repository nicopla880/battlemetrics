import 'package:flutter_test/flutter_test.dart';

import '../../../../../lib/features/servers/data/models/detail_model.dart';
import '../../../../../lib/features/servers/domain/entities/detail.dart';

void main() {
  final tDetailModel = DetailModel(
      official: true,
      headerImage: "www.headerimage.com",
      url: "www.url.com",
      description: "nice server",
      lastWipe: null);

  test('', () {
    // assert
    expect(tDetailModel, isA<Detail>());
  });
}
