import 'package:meta/meta.dart';

import 'detail_model.dart';
import '../../domain/entities/server.dart';

class ServerModel extends Server {
  ServerModel(
      {@required String id,
      @required String name,
      @required String address,
      @required String ip,
      @required int port,
      @required int onlinePlayers,
      @required int maxPlayers,
      @required int rank,
      @required String status,
      @required DetailModel detail,
      @required String createdAt,
      @required String updatedAt});

  @override
  List<Object> get props => [
        id,
        name,
        address,
        ip,
        port,
        onlinePlayers,
        maxPlayers,
        rank,
        status,
        detail,
        createdAt,
        updatedAt
      ];
}
