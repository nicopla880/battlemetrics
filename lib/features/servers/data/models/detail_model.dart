import 'package:meta/meta.dart';

import '../../domain/entities/detail.dart';

class DetailModel extends Detail {
  DetailModel(
      {@required bool official,
      @required String headerImage,
      @required String url,
      @required String description,
      @required String lastWipe});

  List<Object> get props => [official, headerImage, url, description, lastWipe];
}
