import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/server.dart';

abstract class ServerRepository {
  Future<Either<Failure, Server>> getServer(String id);
  Future<Either<Failure, List<Server>>> getServerList();
}
