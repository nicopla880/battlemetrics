import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'detail.dart';

class Server extends Equatable {
  final String id;
  final String name;
  final String address;
  final String ip;
  final int port;
  final int onlinePlayers;
  final int maxPlayers;
  final int rank;
  final String status;
  final Detail detail;
  final DateTime createdAt;
  final DateTime updatedAt;

  Server(
      {@required this.id,
      @required this.name,
      @required this.address,
      @required this.ip,
      @required this.port,
      @required this.onlinePlayers,
      @required this.maxPlayers,
      @required this.rank,
      @required this.status,
      @required this.detail,
      @required this.createdAt,
      @required this.updatedAt});

  @override
  List<Object> get props => [
        id,
        name,
        address,
        ip,
        port,
        onlinePlayers,
        maxPlayers,
        rank,
        status,
        detail,
        createdAt,
        updatedAt
      ];
}
