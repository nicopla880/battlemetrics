import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class Detail extends Equatable {
  final bool official;
  final String headerImage;
  final String url;
  final String description;
  final DateTime lastWipe;

  Detail(
      {@required this.official,
      @required this.headerImage,
      @required this.url,
      @required this.description,
      @required this.lastWipe});

  @override
  List<Object> get props => [official, headerImage, url, description, lastWipe];
}
