import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../entities/server.dart';
import '../repositories/server_repository.dart';

class GetServerById implements UseCase<Server, Params> {
  final ServerRepository repository;

  GetServerById(this.repository);

  @override
  Future<Either<Failure, Server>> call(params) async {
    return await repository.getServer(params.id);
  }
}

class Params extends Equatable {
  final String id;

  Params(this.id);

  @override
  List<Object> get props => [id];
}
