import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../entities/server.dart';
import '../repositories/server_repository.dart';

class GetServerList implements UseCase<List<Server>, NoParams> {
  final ServerRepository repository;

  GetServerList(this.repository);

  @override
  Future<Either<Failure, List<Server>>> call(NoParams params) async {
    return await repository.getServerList();
  }
}
